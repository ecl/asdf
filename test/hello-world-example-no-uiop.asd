#.`(defsystem :hello-world-example-no-uiop
     ;; :build-operation program-op ; this would cause the executable output in same directory :-/
     :class program-system
     :no-uiop t
     :components ((:file "file1"))
     :epilogue-code
     (progn
       (format t "~:[Look ma, no UIOP~;Oops, UIOP~]!~%" (find-package :uiop))
       (format t "~:[But no TEST-PACKAGE :-(~;But TEST-PACKAGE~]!~%" (find-package :test-package))
       #+ecl (si:quit 0) #+mkcl (mk-ext:quit :exit-code 0))
     #+mkcl
     ,@`(;;:prefix-lisp-object-files (,(namestring (truename (translate-logical-pathname #P"SYS:cmp.a"))))
         :extra-build-args ,(or #-windows '(:use-mkcl-shared-libraries nil))
         :class program-system
         :epilogue-code (progn
                          (setq uiop/image:*image-dumped-p* :executable)
                          (setq uiop/image:*lisp-interaction* nil)
                          (uiop/image:restore-image :entry-point (read-from-string "hello:entry-point")))))
