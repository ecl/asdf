
(defsystem hello-world-if-feature
    :serial t
    :components ((:file "hello-if-package")
                 ;; not loaded
                 (:file "hello-if-ignored" :if-feature :no)
                 (:file "hello-if-content")))
